package org.xaltoro.sigtrangateway.ss7.request;

import com.rockville.utilities.XMLReader;
import org.xaltoro.sigtrangateway.Main;

import org.mobicents.protocols.ss7.indicator.NatureOfAddress;
import org.mobicents.protocols.ss7.indicator.RoutingIndicator;
import org.mobicents.protocols.ss7.map.api.MAPApplicationContext;
import org.mobicents.protocols.ss7.map.api.MAPApplicationContextName;
import org.mobicents.protocols.ss7.map.api.MAPApplicationContextVersion;
import org.mobicents.protocols.ss7.map.api.MAPException;
import org.mobicents.protocols.ss7.map.api.MAPParameterFactory;
import org.mobicents.protocols.ss7.map.api.primitives.AddressNature;
import org.mobicents.protocols.ss7.map.api.primitives.ISDNAddressString;
import org.mobicents.protocols.ss7.map.api.primitives.NumberingPlan;
import org.mobicents.protocols.ss7.map.api.primitives.SubscriberIdentity;
import org.mobicents.protocols.ss7.map.api.service.mobility.MAPDialogMobility;
import org.mobicents.protocols.ss7.map.api.service.mobility.subscriberInformation.RequestedInfo;
import org.mobicents.protocols.ss7.sccp.parameter.GlobalTitle;
import org.mobicents.protocols.ss7.sccp.parameter.SccpAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 
public class AnytimeInterrogationRequest extends SS7Requst {

    private static Logger logger = LoggerFactory
            .getLogger("application");
    private static Logger cdr_writer = LoggerFactory.getLogger("cdrs");

    @Override
    public void run() {
        decode();
        sendATIRequest();
    }

    //<methodCall><Refid>abff</Refid><function>AnytimeInterrogationRequest</function><msisdn>923015016175</msisdn></methodCall>
    @Override
    public void decode() {
        XMLReader xRead = new XMLReader();
        xRead.loadXml(RawIncomingXml);
        MSISDN = xRead.readOneElement("msisdn");
        refId = xRead.readOneElement("Refid");
    }

    private void sendATIRequest() {
        logger.debug("[" + refId + "] Preparing MAP ATI request for msisdn: " + MSISDN);
        MAPParameterFactory mapParameterFactory = Main.mapProvider
                .getMAPParameterFactory();
        MAPApplicationContext mapGWAppContext = MAPApplicationContext
                .getInstance(MAPApplicationContextName.anyTimeEnquiryContext,
                MAPApplicationContextVersion.version3);
        //String hlrGT = "";
        //hlrGT = ConfigReader.GetProperty("hlr_gt");
        logger.debug("trying with hlr_gt: " + MSISDN);
        GlobalTitle called_gt = GlobalTitle
                .getInstance(
                0,
                org.mobicents.protocols.ss7.indicator.NumberingPlan.ISDN_TELEPHONY,
                NatureOfAddress.INTERNATIONAL, MSISDN);
        GlobalTitle calling_gt = GlobalTitle
                .getInstance(
                0,
                org.mobicents.protocols.ss7.indicator.NumberingPlan.ISDN_TELEPHONY,
                NatureOfAddress.INTERNATIONAL, service_center_address_map);
        SccpAddress called_sccp_address = new SccpAddress(
                RoutingIndicator.ROUTING_BASED_ON_GLOBAL_TITLE, remote_pc,
                called_gt, hlr_ssn);
        SccpAddress calling_sccp_address = new SccpAddress(
                RoutingIndicator.ROUTING_BASED_ON_GLOBAL_TITLE, local_pc,
                calling_gt, local_map_ssn);
        try {
            MAPDialogMobility curDialog = Main.mapProvider.getMAPServiceMobility()
                    .createNewDialog(mapGWAppContext, calling_sccp_address,
                    null, called_sccp_address, null);

            ISDNAddressString msisdn = mapParameterFactory
                    .createISDNAddressString(
                    AddressNature.international_number,
                    NumberingPlan.ISDN, MSISDN);
            ISDNAddressString gsmSCFAddress = mapParameterFactory
                    .createISDNAddressString(
                    AddressNature.international_number,
                    NumberingPlan.ISDN, service_center_address_map);
            SubscriberIdentity subscriberIdentity = mapParameterFactory
                    .createSubscriberIdentity(msisdn);
            RequestedInfo requestedInfo = mapParameterFactory
                    .createRequestedInfo(true, true, null, true, null, false,
                    false, false);

            long InvokeId = curDialog.addAnyTimeInterrogationRequest(
                    subscriberIdentity, requestedInfo, gsmSCFAddress, null);
            Main.HT_SS7Requests.put(curDialog.getLocalDialogId(), this);
            curDialog.send();
            cdr_writer.info(this.refId + "," + MSISDN + ",OUT,ATI-REQUEST");
            logger.info("[" + refId + "] Sent MAP ATI for MSISDN: " + MSISDN + " Dialog Id: " + curDialog.getLocalDialogId());
        } catch (MAPException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
