package org.xaltoro.sigtrangateway.utils;

/**
 *
 * @author Shoaib
 */
public enum CamelState {

    CamelBusy, NotReachable, AssumedIdle, Error
};
