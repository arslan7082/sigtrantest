package org.xaltoro.sigtrangateway.utils;

public class ATIState {

    public ATIState() {
        AgeOfLocation = -1;
        AtiCamelState = CamelState.Error;
    }
    /**
     *
     * @author admin
     */
    public int AgeOfLocation;
    public CamelState AtiCamelState;

    public void setAgeOfLocation(String value) {
        try {
            AgeOfLocation = Integer.parseInt(value);
        } catch (Exception exp) {
        }
    }

    @Override
    public String toString() {
        
        return "<ATIState><CamelState>" + AtiCamelState + "</CamelState><ageOfLocation>" + AgeOfLocation + "</ageOfLocation></ATIState>";
    }
}
