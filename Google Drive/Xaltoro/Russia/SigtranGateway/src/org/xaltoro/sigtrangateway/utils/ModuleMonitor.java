/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.xaltoro.sigtrangateway.utils;

import org.xaltoro.sigtrangateway.Main;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Arslan
 */
public class ModuleMonitor implements Runnable {

    private static org.slf4j.Logger logger = LoggerFactory
            .getLogger("application");

    @Override
    public void run() {
        while (true) {
            int mb = 1024 * 1024;
            Runtime runtime = Runtime.getRuntime();
            long memoryUsed = (runtime.totalMemory() - runtime.freeMemory()) / mb;
            long maxMemory = runtime.maxMemory() / mb;
            logger.info("Memory Used:" + memoryUsed + " MB / " + maxMemory + " MB");


            int nbRunning = 0;
            for (Thread t : Thread.getAllStackTraces().keySet()) {
                if (t.getState() == Thread.State.RUNNABLE) {
                    nbRunning++;
                }
            }
            logger.info("Threads Used:" + nbRunning);
            try {
                Thread.sleep(60000);
            } catch (InterruptedException ex) {
                logger.error(ex.getLocalizedMessage());
            }
        }
    }
}
