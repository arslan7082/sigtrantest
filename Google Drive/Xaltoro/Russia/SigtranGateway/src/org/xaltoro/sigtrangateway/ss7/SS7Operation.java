/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.xaltoro.sigtrangateway.ss7;

import com.rockville.properties.PropertyNotFoundException;
import com.rockville.properties.PropertyReader;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pc
 */
public class SS7Operation {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger(SS7Operation.class);

    public int local_pc;
    public int remote_pc;
    public String service_center_address_map;
    public String service_center_address_cap;
    public String scp_gt;
    public int hlr_ssn;
    public int msc_ssn;
    public int camel_ssn;
    public int local_map_ssn;
    public String rawJson;

    public SS7Operation() {
        try {
            this.local_pc = PropertyReader.GetPropertyAsInt("local_pc");
            this.remote_pc = PropertyReader.GetPropertyAsInt("remote_pc");

            this.service_center_address_map = PropertyReader.GetProperty("service_center_address_map");
            this.service_center_address_cap = PropertyReader.GetProperty("service_center_address_cap");
            this.scp_gt = PropertyReader.GetProperty("scp_gt");
            
            this.hlr_ssn = PropertyReader.GetPropertyAsInt("hlr_ssn");
            this.msc_ssn = PropertyReader.GetPropertyAsInt("msc_ssn");
            this.camel_ssn = PropertyReader.GetPropertyAsInt("camel_ssn");
            this.local_map_ssn = PropertyReader.GetPropertyAsInt("local_map_ssn");
        } catch (PropertyNotFoundException ex) {
            logger.error(ex.getMessage());
        }
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty("line.separator");

        result.append(this.getClass().getName());
        result.append(" Object {");
        result.append(newLine);

        //determine fields declared in this class only (no fields of superclass)
        Field[] fields = this.getClass().getDeclaredFields();

        //print field names paired with their values
        for (Field field : fields) {
            result.append("  ");
            try {
                result.append(field.getName());
                result.append(": ");
                //requires access to private field:
                result.append(field.get(this));
            } catch (IllegalAccessException ex) {
                System.out.println(ex);
            }
            result.append(newLine);
        }
        result.append("}");

        return result.toString();
    }
}
