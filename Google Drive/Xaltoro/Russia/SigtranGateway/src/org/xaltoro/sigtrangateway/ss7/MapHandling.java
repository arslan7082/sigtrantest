package org.xaltoro.sigtrangateway.ss7;

import org.xaltoro.sigtrangateway.Main;
import org.xaltoro.sigtrangateway.ss7.request.SS7Requst;
import org.xaltoro.sigtrangateway.ss7.response.AnytimeInterrogationResponse;
import org.xaltoro.sigtrangateway.ss7.response.SS7ResponseError;

import org.mobicents.protocols.ss7.map.api.MAPDialog;
import org.mobicents.protocols.ss7.map.api.MAPDialogListener;
import org.mobicents.protocols.ss7.map.api.MAPMessage;
import org.mobicents.protocols.ss7.map.api.MAPProvider;
import org.mobicents.protocols.ss7.map.api.dialog.MAPAbortProviderReason;
import org.mobicents.protocols.ss7.map.api.dialog.MAPAbortSource;
import org.mobicents.protocols.ss7.map.api.dialog.MAPNoticeProblemDiagnostic;
import org.mobicents.protocols.ss7.map.api.dialog.MAPRefuseReason;
import org.mobicents.protocols.ss7.map.api.dialog.MAPUserAbortChoice;
import org.mobicents.protocols.ss7.map.api.errors.MAPErrorMessage;
import org.mobicents.protocols.ss7.map.api.primitives.AddressString;
import org.mobicents.protocols.ss7.map.api.primitives.IMSI;
import org.mobicents.protocols.ss7.map.api.primitives.MAPExtensionContainer;
import org.mobicents.protocols.ss7.map.api.service.callhandling.MAPServiceCallHandlingListener;
import org.mobicents.protocols.ss7.map.api.service.callhandling.ProvideRoamingNumberRequest;
import org.mobicents.protocols.ss7.map.api.service.callhandling.ProvideRoamingNumberResponse;
import org.mobicents.protocols.ss7.map.api.service.callhandling.SendRoutingInformationRequest;
import org.mobicents.protocols.ss7.map.api.service.callhandling.SendRoutingInformationResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.MAPServiceMobilityListener;
import org.mobicents.protocols.ss7.map.api.service.mobility.authentication.SendAuthenticationInfoRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.authentication.SendAuthenticationInfoResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.imei.CheckImeiRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.imei.CheckImeiResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.CancelLocationRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.CancelLocationResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.PurgeMSRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.PurgeMSResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.SendIdentificationRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.SendIdentificationResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.UpdateGprsLocationRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.UpdateGprsLocationResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.UpdateLocationRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.locationManagement.UpdateLocationResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.subscriberInformation.AnyTimeInterrogationRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.subscriberInformation.AnyTimeInterrogationResponse;
import org.mobicents.protocols.ss7.map.api.service.mobility.subscriberManagement.InsertSubscriberDataRequest;
import org.mobicents.protocols.ss7.map.api.service.mobility.subscriberManagement.InsertSubscriberDataResponse;
import org.mobicents.protocols.ss7.map.api.service.sms.AlertServiceCentreRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.AlertServiceCentreResponse;
import org.mobicents.protocols.ss7.map.api.service.sms.ForwardShortMessageRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.ForwardShortMessageResponse;
import org.mobicents.protocols.ss7.map.api.service.sms.InformServiceCentreRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.MAPServiceSmsListener;
import org.mobicents.protocols.ss7.map.api.service.sms.MoForwardShortMessageRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.MoForwardShortMessageResponse;
import org.mobicents.protocols.ss7.map.api.service.sms.MtForwardShortMessageRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.MtForwardShortMessageResponse;
import org.mobicents.protocols.ss7.map.api.service.sms.ReportSMDeliveryStatusRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.ReportSMDeliveryStatusResponse;
import org.mobicents.protocols.ss7.map.api.service.sms.SendRoutingInfoForSMRequest;
import org.mobicents.protocols.ss7.map.api.service.sms.SendRoutingInfoForSMResponse;
import org.mobicents.protocols.ss7.map.api.service.supplementary.MAPServiceSupplementaryListener;
import org.mobicents.protocols.ss7.map.api.service.supplementary.ProcessUnstructuredSSRequest;
import org.mobicents.protocols.ss7.map.api.service.supplementary.ProcessUnstructuredSSResponse;
import org.mobicents.protocols.ss7.map.api.service.supplementary.UnstructuredSSNotifyRequest;
import org.mobicents.protocols.ss7.map.api.service.supplementary.UnstructuredSSNotifyResponse;
import org.mobicents.protocols.ss7.map.api.service.supplementary.UnstructuredSSRequest;
import org.mobicents.protocols.ss7.map.api.service.supplementary.UnstructuredSSResponse;
import org.mobicents.protocols.ss7.tcap.asn.ApplicationContextName;
import org.mobicents.protocols.ss7.tcap.asn.comp.Problem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MapHandling implements MAPDialogListener,
        MAPServiceSmsListener, MAPServiceMobilityListener, MAPServiceCallHandlingListener, MAPServiceSupplementaryListener {

    private static Logger logger = LoggerFactory
            .getLogger("application");

    public MapHandling(MAPProvider mapProvider) {
    }

    @Override
    public void onErrorComponent(MAPDialog arg0, Long arg1, MAPErrorMessage arg2) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onInvokeTimeout(MAPDialog arg0, Long arg1) {
        logger.debug("onInvokeTimeout Called for InvokeID:" + arg0.getLocalDialogId());
        SS7Requst ss7Request = (SS7Requst) Main.HT_SS7Requests.get(arg0.getLocalDialogId());
        if (ss7Request != null) {
            ss7Request.ReturnXML = "<error>rejected</error>";
            synchronized (ss7Request) {
                ss7Request.notify();
            }
            Main.HT_SS7Requests.remove(arg0.getLocalDialogId());
        }

    }

    @Override
    public void onMAPMessage(MAPMessage arg0) {
        // TODO Auto-generated method stub
        logger.debug("onMAPMessage Called for InvokeID:" + arg0.getInvokeId());

    }

    @Override
    public void onRejectComponent(MAPDialog arg0, Long arg1, Problem arg2,
            boolean arg3) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onAlertServiceCentreRequest(AlertServiceCentreRequest arg0) {

    }

    @Override
    public void onAlertServiceCentreResponse(AlertServiceCentreResponse arg0) {
        logger.debug("AlertSC - onAlertServiceCentreResponse Called for InvokeID:" + arg0.getInvokeId());
    }

    @Override
    public void onForwardShortMessageRequest(ForwardShortMessageRequest arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onForwardShortMessageResponse(ForwardShortMessageResponse arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onInformServiceCentreRequest(InformServiceCentreRequest arg0) {
        logger.debug("onInformServiceCentreRequest Called for InvokeID:" + arg0.getInvokeId());
    }

    @Override
    public void onMoForwardShortMessageRequest(MoForwardShortMessageRequest arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onMoForwardShortMessageResponse(
            MoForwardShortMessageResponse arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onMtForwardShortMessageRequest(MtForwardShortMessageRequest arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onMtForwardShortMessageResponse(MtForwardShortMessageResponse arg0) {

    }

    @Override
    public void onReportSMDeliveryStatusRequest(
            ReportSMDeliveryStatusRequest arg0) {
        logger.debug("onReportSMDeliveryStatusRequest Called for DialogID:" + arg0.getInvokeId());

    }

    @Override
    public void onReportSMDeliveryStatusResponse(ReportSMDeliveryStatusResponse arg0) {

    }

    @Override
    public void onSendRoutingInfoForSMRequest(SendRoutingInfoForSMRequest arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSendRoutingInfoForSMResponse(SendRoutingInfoForSMResponse arg0) {
           }

    @Override
    public void onDialogAccept(MAPDialog arg0, MAPExtensionContainer arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDialogClose(MAPDialog arg0) {
        logger.debug("onDialogClose Called for DialogID:" + arg0.getLocalDialogId());
    }

    @Override
    public void onDialogDelimiter(MAPDialog arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDialogNotice(MAPDialog arg0, MAPNoticeProblemDiagnostic arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDialogProviderAbort(MAPDialog arg0,
            MAPAbortProviderReason arg1, MAPAbortSource arg2,
            MAPExtensionContainer arg3) {
        long localDialogID = -1;
        long remoteDialogID = -1;
        String abortProviderReason = "";
        String abortSource = "";

        String acn = "";
        String remoteSccpAddress = "";
        String localSccpAddress = "";
        try {
            localDialogID = arg0.getLocalDialogId();
        } catch (Exception exp) {
        }
        try {
            remoteDialogID = arg0.getRemoteDialogId();
        } catch (Exception exp) {
        }
        try {
            remoteSccpAddress = arg0.getRemoteAddress().toString();
            localSccpAddress = arg0.getLocalAddress().toString();
            acn = arg0.getApplicationContext().toString();
            if (arg1 != null) {
                abortProviderReason = arg1.toString();
            }
            abortSource = arg2.toString();

        } catch (Exception exp) {
        }
        logger.info("MAP-onDialogProviderAbort dID:" + localDialogID + " rdID: " + remoteDialogID + " SCCPAdd: " + localSccpAddress + " rSCCPAdd: " + remoteSccpAddress + " acn: " + acn + " abortProviderReason: " + abortProviderReason + " abortSource: " + abortSource);
    }

    @Override
    public void onDialogReject(MAPDialog arg0, MAPRefuseReason arg1,
            ApplicationContextName arg2, MAPExtensionContainer arg3) {
        logger.info("onDialogReject Called for DialogID:" + arg0.getLocalDialogId() + " RefuseReason:" + arg1.toString());
        String reason = "";
        if (arg1 != null) {
            reason = arg1.toString();
        }
        SS7Requst submitSMRequest = (SS7Requst) Main.HT_PendingSubmitSMRequests.get(arg0.getLocalDialogId());
        if (submitSMRequest != null) {
            submitSMRequest.ReturnXML = "<error>rejected</error><reason>" + reason + "</reason>";
            synchronized (submitSMRequest) {
                submitSMRequest.notify();
            }
            Main.HT_PendingSubmitSMRequests.remove(arg0);
        }
        SS7Requst ss7Request = (SS7Requst) Main.HT_SS7Requests.get(arg0.getLocalDialogId());
        if (ss7Request != null) {
            ss7Request.ReturnXML = "<error>rejected</error>";
            synchronized (ss7Request) {
                ss7Request.notify();
            }
            Main.HT_SS7Requests.remove(arg0.getLocalDialogId());
        }

    }

    @Override
    public void onDialogRelease(MAPDialog arg0) {
        logger.debug("onDialogRelease Called for DialogID:" + arg0.getLocalDialogId() + " removing requests from hashtables");
        org.xaltoro.sigtrangateway.ss7.response.SS7ResponseError ss7Release = new SS7ResponseError();
        ss7Release.DialogId = arg0.getLocalDialogId();
        Thread atiResponseThread = new Thread(ss7Release);
        atiResponseThread.start();

    }

    @Override
    public void onDialogRequest(MAPDialog arg0, AddressString arg1,
            AddressString arg2, MAPExtensionContainer arg3) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDialogRequestEricsson(MAPDialog arg0, AddressString arg1,
            AddressString arg2, IMSI arg3, AddressString arg4) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onDialogTimeout(MAPDialog arg0) {
        long localDialogID = -1;
        long remoteDialogID = -1;
        try {
            localDialogID = arg0.getLocalDialogId();
        } catch (Exception exp) {
        }
        try {
            remoteDialogID = arg0.getRemoteDialogId();
        } catch (Exception exp) {
        }
        logger.info("MAP - onDialogTimeout Called for DialogID:" + localDialogID + " RemoteDialogID: " + remoteDialogID);

      
    }

    @Override
    public void onDialogUserAbort(MAPDialog arg0, MAPUserAbortChoice arg1,
            MAPExtensionContainer arg2) {
        long localDialogID = -1;
        long remoteDialogID = -1;
        String genAbortReason = "";
        String userAbortChoice = "";
        String acn = "";
        String remoteSccpAddress = "";
        String localSccpAddress = "";
        try {
            localDialogID = arg0.getLocalDialogId();
        } catch (Exception exp) {
        }
        try {
            remoteDialogID = arg0.getRemoteDialogId();
        } catch (Exception exp) {
        }
        try {
            remoteSccpAddress = arg0.getRemoteAddress().toString();
            localSccpAddress = arg0.getLocalAddress().toString();
            acn = arg0.getApplicationContext().toString();
            if (arg1 != null) {
                userAbortChoice = arg1.toString();
            }
        } catch (Exception exp) {
        }
        logger.info("MAP-onDialogUserAbort dID:" + localDialogID + " rdID: " + remoteDialogID + " SCCPAdd: " + localSccpAddress + " rSCCPAdd: " + remoteSccpAddress + " acn: " + acn + " userAbortChoice: " + userAbortChoice);
    }

    @Override
    public void onUpdateLocationRequest(UpdateLocationRequest ulr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onUpdateLocationResponse(UpdateLocationResponse ulr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCancelLocationRequest(CancelLocationRequest clr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCancelLocationResponse(CancelLocationResponse clr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSendIdentificationRequest(SendIdentificationRequest sir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSendIdentificationResponse(SendIdentificationResponse sir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onUpdateGprsLocationRequest(UpdateGprsLocationRequest uglr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onUpdateGprsLocationResponse(UpdateGprsLocationResponse uglr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSendAuthenticationInfoRequest(SendAuthenticationInfoRequest sair) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSendAuthenticationInfoResponse(SendAuthenticationInfoResponse sair) {
        logger.debug("onSendAuthenticationInfoResponse Called:" + sair.toString());

    }

    @Override
    public void onAnyTimeInterrogationRequest(AnyTimeInterrogationRequest atir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onAnyTimeInterrogationResponse(AnyTimeInterrogationResponse atir) {

        logger.debug("MAP ATI response received for dialog id:" + atir.getMAPDialog().getLocalDialogId());
        org.xaltoro.sigtrangateway.ss7.response.AnytimeInterrogationResponse ati = new AnytimeInterrogationResponse();
        ati.DialogId = atir.getMAPDialog().getLocalDialogId();
        ati.subState = atir.getSubscriberInfo().getSubscriberState().getSubscriberStateChoice().toString();

        try {
            ati.ageOfLocation = Integer.toString(atir.getSubscriberInfo().getLocationInformation().getAgeOfLocationInformation());
            ati.vlr = atir.getSubscriberInfo().getLocationInformation().getVlrNumber().getAddress();
            ati.cellId = atir.getSubscriberInfo().getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().getCellGlobalIdOrServiceAreaIdFixedLength().getCellIdOrServiceAreaCode();
            ati.mcc = atir.getSubscriberInfo().getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().getCellGlobalIdOrServiceAreaIdFixedLength().getMCC();
            ati.mnc = atir.getSubscriberInfo().getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().getCellGlobalIdOrServiceAreaIdFixedLength().getMNC();
            ati.lac = atir.getSubscriberInfo().getLocationInformation().getCellGlobalIdOrServiceAreaIdOrLAI().getCellGlobalIdOrServiceAreaIdFixedLength().getLac();

        } catch (Exception e) {
            logger.error("Exception setting VLR, cellId, mcc,mnc,lac :" + e.getMessage());
        }
        Thread atiResponseThread = new Thread(ati);
        atiResponseThread.start();
        atir.getMAPDialog().release();
    }

    @Override
    public void onInsertSubscriberDataRequest(InsertSubscriberDataRequest isdr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onInsertSubscriberDataResponse(InsertSubscriberDataResponse isdr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCheckImeiRequest(CheckImeiRequest cir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onCheckImeiResponse(CheckImeiResponse cir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSendRoutingInformationRequest(SendRoutingInformationRequest srir) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onSendRoutingInformationResponse(SendRoutingInformationResponse srir) {
       

    }

    @Override
    public void onProvideRoamingNumberRequest(ProvideRoamingNumberRequest prnr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onProvideRoamingNumberResponse(ProvideRoamingNumberResponse prnr) {
        logger.error("onProvideRoamingNumberResponse Not supported yet.");
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onProcessUnstructuredSSResponse(ProcessUnstructuredSSResponse pussr) {
        logger.error("ProcessUnstructuredSSResponse Not supported yet.");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUnstructuredSSRequest(UnstructuredSSRequest ussr) {
        logger.error("onUnstructuredSSRequest Not supported yet.");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUnstructuredSSResponse(UnstructuredSSResponse ussr) {
      
    }

    @Override
    public void onUnstructuredSSNotifyRequest(UnstructuredSSNotifyRequest ussnr) {
        logger.error("USSD onUnstructuredSSNotifyRequest Not supported yet.");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUnstructuredSSNotifyResponse(UnstructuredSSNotifyResponse ussnr) {
        logger.error("USSD onUnstructuredSSNotifyResponse Not supported yet.");
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onProcessUnstructuredSSRequest(ProcessUnstructuredSSRequest procUnstrReqInd) {
        logger.debug("USSD Spawning new thread onProcessUnstructuredSSRequest for dialogId: " + procUnstrReqInd.getMAPDialog().getLocalDialogId() + " USSD String:" + procUnstrReqInd.getUSSDString());

       
    }

    @Override
    public void onPurgeMSRequest(PurgeMSRequest pmsr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onPurgeMSResponse(PurgeMSResponse pmsr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
