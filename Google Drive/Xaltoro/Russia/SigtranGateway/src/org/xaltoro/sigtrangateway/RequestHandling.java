package org.xaltoro.sigtrangateway;

//import RequestFactoryProvider.IBankFactory;
import com.rockville.utilities.XMLReader;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.Callable;
import org.xaltoro.sigtrangateway.ss7.request.SS7Requst;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author admin
 */
public class RequestHandling implements Callable<String> {

    private static Logger logger = LoggerFactory.getLogger("application");
    Socket mClientSocket;
    private boolean mClientConnected = false;
    private DataOutputStream outToClient;
    private BufferedReader inFromClient;
    String RefID;

    public RequestHandling(Socket clientSocket) {
        this.mClientSocket = clientSocket;
        this.mClientConnected = true;
    }

    private Object LoadClass(String completeClassName) {
        ClassLoader classLoader = this.getClass().getClassLoader();
        Class aClass = null;
        try {
            aClass = classLoader.loadClass(completeClassName);
            System.out.println("aClass.getName() = " + aClass.getName());
        } catch (ClassNotFoundException e) {
            logger.error("Could Not Load Request Class: " + completeClassName);
        }
        SS7Requst reqFactoryObj = null;
        try {
            reqFactoryObj = (SS7Requst) aClass.newInstance();
        } catch (InstantiationException ex) {
            logger.error("Error in instantiating request: " + ex.getMessage());
        } catch (IllegalAccessException ex) {
            logger.error("Error in accessing request class: " + ex.getMessage());
        }
        return reqFactoryObj;
    }

    @Override
    public String call() throws Exception {
        logger.debug("Incoming request - connected...");
        outToClient = null;
        RefID = "";
        try {
            while (this.mClientConnected) {
                logger.debug("this.mClientSocket.isClosed(): " + this.mClientSocket.isClosed());
                logger.debug("this.mClientSocket.isConnected(): " + this.mClientSocket.isConnected());
                logger.debug("this.mClientSocket.isInputShutdown(): " + this.mClientSocket.isInputShutdown());
                if (this.mClientSocket.isOutputShutdown()) {
                    this.mClientConnected = false;
                } else {
                    inFromClient = new BufferedReader(new InputStreamReader(this.mClientSocket.getInputStream()));
                    outToClient = new DataOutputStream(this.mClientSocket.getOutputStream());

                    String xml = inFromClient.readLine();
                    //String xml = "<methodCall><function>SendRoutingInfoRequest</function><MSISDN>923315134448</MSISDN></methodCall>";
                    //String xml = "<methodCall><function>SubmitSMRequest</function><aMsisdn>923315134448</aMsisdn><bMsisdn>923008507006</bMsisdn><content>This is test Message</content><smsType>flash</smsType> <!--Possible Values flash/sms--><encodingBitLen>7</encodingBitLen></methodCall>";


                    XMLReader xmlReq = new XMLReader();
                    xmlReq.loadXml(xml);
                    try {
                        RefID = xmlReq.readOneElement("refID");
                    } catch (Exception exp) {
                        RefID = "ERROR-NoRefID";
                    }
                    if (RefID.trim() == "" || RefID.trim() == "null") {
                        try {
                            RefID = xmlReq.readOneElement("Refid");
                        } catch (Exception exp) {
                            RefID = "ERROR-NoRefID";
                        }
                    }
                    logger.info("[" + RefID + "] Incoming XML [" + xml + "]");

                    String functionName = xmlReq.readOneElement("function");
                    String classPackageName = "org.xaltoro.sigtrangateway.ss7.request";
                    String ClassCompleteName = classPackageName + "." + functionName;

                    final SS7Requst reqFactoryObj = (SS7Requst) LoadClass(ClassCompleteName);
                    reqFactoryObj.RawIncomingXml = xml;
                    //reqFactoryObj.run();
                    logger.debug("[" + RefID + "] Forking a new thread");
                    new Thread() {
                        public void run() {
                            reqFactoryObj.run();
                        }
                    }.start();

                    synchronized (reqFactoryObj) {
                        try {
                            // Calling wait() will block this thread until another thread calls notify() on the object.
                            logger.debug("[" + RefID + "] Started waiting ");
                            reqFactoryObj.wait();
                            logger.debug("[" + RefID + "] Finished waiting");
                        } catch (InterruptedException e) {
                            // Happens if someone interrupts your thread.
                            logger.error("[" + RefID + "] Error on waiting on current thread, xml:" + xml);
                        }
                    }

                    String xmlResponse = reqFactoryObj.ReturnXML + "\n\n";
                    outToClient.writeBytes(xmlResponse);
                    logger.info("[" + RefID + "] Response: " + xmlResponse);
                    //outToClient.flush(); //Removed this line to cater the Broken pipe error
                    Thread.sleep(10); //Added this sleep to cater the Broken pipe error that came when MapGW tried sending somthing to MapProxy, It sent it successfully from here but MapProxy got a broken pipe error while recieving the data
                    //logger.info("[" + RefID + "] Response xml: [" + xmlResponse + "]");
                    this.mClientConnected = false;
                }
            }

        } catch (IOException | InterruptedException e) {
            logger.error("[" + RefID + "]" + e.getMessage(), e);
            cleanupSocket();
        } finally {
            cleanupSocket();
        }
        //logger.debug("getting out");
        return "";
    }

    private void cleanupSocket() {
        try {
            this.mClientSocket.getOutputStream().close();
            outToClient.flush();
            outToClient.close();
            this.mClientSocket.close();
        } catch (IOException ex) {
            logger.error("[" + RefID + "]" + ex.getMessage(), ex);
        }

    }
}