/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.xaltoro.sigtrangateway.ss7.response;

import org.xaltoro.sigtrangateway.Main;
import org.xaltoro.sigtrangateway.ss7.request.SS7Requst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pc
 */
public class SS7ResponseError extends SS7Response {

    private static Logger logger = LoggerFactory
            .getLogger("application");

    @Override
    public String encode() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return "";
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);//Sleeping and letting the actual valid response class handling the response
        } catch (InterruptedException ex) {
            logger.error(ex.getMessage());
        }
        SS7Requst submitSMRequest = (SS7Requst) Main.HT_PendingSubmitSMRequests.get(DialogId);
        if (submitSMRequest != null) {
            if (submitSMRequest.isComplete) {
                return;
            }
            //It has come down to clean up now, the request was a failure
            logger.info("Removing request from HT_PendingSubmitSMRequests");
            submitSMRequest.ReturnXML = "<error>request rejected</error>";
            synchronized (submitSMRequest) {
                submitSMRequest.notify();
            }
            Main.HT_PendingSubmitSMRequests.remove(DialogId);
        }
        SS7Requst ss7Request = (SS7Requst) Main.HT_SS7Requests.get(DialogId);
        if (ss7Request != null) {
            if (ss7Request.isComplete) {
                return;//Request already handled by the correct class
            }
            //It has come down to clean up now, the request was a failure
            logger.info("Removing request from HT_SS7Requests");
            ss7Request.ReturnXML = "<error>request rejected</error>";
            synchronized (ss7Request) {
                ss7Request.notify();
            }
            Main.HT_SS7Requests.remove(DialogId);
        }
    }
}
