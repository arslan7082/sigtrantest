package org.xaltoro.sigtrangateway;


import com.rockville.memcache.MemcacheClient;
import com.rockville.properties.PropertyNotFoundException;
import com.rockville.properties.PropertyReader;
import java.io.IOException;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import net.rubyeye.xmemcached.utils.AddrUtil;
import org.xaltoro.sigtrangateway.utils.ModuleMonitor;

import org.mobicents.protocols.api.IpChannelType;
import org.mobicents.protocols.sctp.ManagementImpl;
import org.mobicents.protocols.ss7.cap.CAPStackImpl;
import org.mobicents.protocols.ss7.cap.api.CAPProvider;
import org.mobicents.protocols.ss7.m3ua.impl.M3UAManagementImpl;
import org.mobicents.protocols.ss7.map.MAPStackImpl;
import org.mobicents.protocols.ss7.map.api.MAPProvider;

import org.mobicents.protocols.ss7.sccp.impl.SccpStackImpl;
import org.mobicents.protocols.ss7.tcap.TCAPStackImpl;
import org.mobicents.protocols.ss7.tcap.api.TCAPStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xaltoro.sigtrangateway.ss7.MapHandling;

public class Main {

    public static Hashtable HT_SS7Requests;
    public static Hashtable HT_PendingSubmitSMRequests;
    public static Hashtable HT_Idps;

    private static Logger logger = LoggerFactory
            .getLogger("application");
    // SCTP
    private ManagementImpl sctpManagement;
    // M3UA
    private M3UAManagementImpl clientM3UAMgmt;
    // SCCP
    private SccpStackImpl sccpStack_map;
    // TCAP
    private TCAPStack tcapStack_map;
    // CAP
    private CAPStackImpl capStack;
    public static CAPProvider capProvider;
    // MAP
    private MAPStackImpl mapStack;
    public static MAPProvider mapProvider;
    private String SS7_CONF_FILE_PATH;
    //MAPGW Listener
    private ServerSocket mListenSocket;
    private int mPort;
    private int mMaxClients;
    ScheduledExecutorService mRequestPool;

    /**
     * @param args
     * @throws java.io.IOException
     * @throws com.rockville.properties.PropertyNotFoundException
     */
    public static void main(String[] args) throws IOException, PropertyNotFoundException, Exception {

        HT_SS7Requests = new Hashtable();
        HT_PendingSubmitSMRequests = new Hashtable();

        HT_Idps = new Hashtable();
        Main m = new Main();
        String configFileName = "capgw.properties";
        PropertyReader.Init(configFileName);


      
        try {
            //Initializing Configreader

            //Starting module monitor thread
            ModuleMonitor mm = new ModuleMonitor();
            Thread monitoringThread = new Thread(mm);
            monitoringThread.start();
            //Starting SIGTRAN stack
            m.initializeStack(IpChannelType.SCTP);
            //Initializing memcache
                  

         

         
            //Starting TCP Listener
            logger.info("Initializing TCP Listener");
            int threadPoolSize = PropertyReader.GetPropertyAsInt("Client.ThreadPoolSize");
            m.mMaxClients = threadPoolSize;
            m.mRequestPool = Executors.newScheduledThreadPool(m.mMaxClients);
            m.mListenSocket = new ServerSocket();
            m.startListener();

        } catch (PropertyNotFoundException pe) {
            logger.error(pe.getMessage(), pe);
        }
    }

    protected void startListener() {
        try {
            String mapgwPort = PropertyReader.GetProperty("tcp.port", "9000");// myResources.getProperty("tcp.port");
            String mapgwIP = PropertyReader.GetProperty("tcp.ip", "localhost");
            this.mListenSocket.bind(new InetSocketAddress(mapgwIP, Integer.parseInt(mapgwPort)));
            //this.mListenSocket.bind(new  InetSocketAddress("127.0.0.1", 9000));
            logger.info("MAPGW listening at ip:" + mapgwIP + " port: " + mapgwPort);
            while (true) {
                Socket clientSocket = this.mListenSocket.accept();
                clientSocket.setSoTimeout(PropertyReader.GetPropertyAsInt("tcp.readTimeoutMilliseconds"));
                RequestHandling req = new RequestHandling(clientSocket);

                final Future handler = mRequestPool.submit(req);
                mRequestPool.schedule(new Runnable() {
                    public void run() {
                        handler.cancel(true);
                    }
                }, PropertyReader.GetPropertyAsInt("threadTimeout"), TimeUnit.MILLISECONDS);
                // this.mRequestPool.execute(req);
                logger.debug("Finished executing");
            }
        } catch (BindException ex) {
            logger.error(ex.getMessage(), ex);
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {

            logger.info("Shutting down TCP Server...");
            try {
                logger.info("Sleeping for 10000 and attempting to start TCP server again");
                Thread.sleep(10000);
                startListener();
            } catch (InterruptedException ex) {
                logger.error("Exception while sleeping");
            }

        }
    }

    protected void initializeStack(IpChannelType ipChannelType)
            throws Exception {

        SS7_CONF_FILE_PATH = PropertyReader.GetProperty("SS7_CONF_FILE_PATH");
        System.out.println(SS7_CONF_FILE_PATH);
        this.initSCTP(ipChannelType);

        // Initialize M3UA first
        this.initM3UA();

        // Initialize SCCP
        this.initSCCP();

        // Initialize TCAP
        this.initTCAP();

        // initialize MAP
        this.initMAP();

        // Initialize CAP
       // this.initCAP();

        // FInally start ASP
        // Set 5: Finally start ASP
        // this.clientM3UAMgmt.startAsp("ASP1");
    }

    private void initSCTP(IpChannelType ipChannelType) throws Exception {
        logger.debug("Initializing SCTP Stack ....");
        this.sctpManagement = new ManagementImpl("SCTPManagement");
        this.sctpManagement.setConnectDelay(0);
        this.sctpManagement.setSingleThread(false);

        this.sctpManagement.setPersistDir(SS7_CONF_FILE_PATH);

        this.sctpManagement.start();
        logger.debug("Initialized SCTP Stack ....");
    }

    private void initM3UA() throws Exception {
        logger.debug("Initializing M3UA Stack ....");
        this.clientM3UAMgmt = new M3UAManagementImpl("Mtp3UserPart");
        this.clientM3UAMgmt.setTransportManagement(this.sctpManagement);
        this.clientM3UAMgmt.setPersistDir(SS7_CONF_FILE_PATH);

        this.clientM3UAMgmt.start();
        logger.debug("Initialized M3UA Stack ....");
    }

    private void initSCCP() throws Exception {
        logger.debug("Initializing SCCP Stack ....");
        this.sccpStack_map = new SccpStackImpl("SccpStack");
        this.sccpStack_map.setMtp3UserPart(1, this.clientM3UAMgmt);
        this.sccpStack_map.setPersistDir(SS7_CONF_FILE_PATH);

        //this.sccpStack.start();
        this.sccpStack_map.start();
        logger.debug("Initialized SCCP Stack ....");
    }

    private void initTCAP() {
        logger.debug("Initializing TCAP Stack ....");
        this.tcapStack_map = new TCAPStackImpl("tcap_map", this.sccpStack_map.getSccpProvider(),
                PropertyReader.GetPropertyAsInt("local_map_ssn"));
     
        try {
            this.tcapStack_map.setMaxDialogs(PropertyReader.GetPropertyAsInt("tcap.max_dialogs"));
            this.tcapStack_map.setInvokeTimeout(PropertyReader.GetPropertyAsInt("InvokeTimeout_map"));
            this.tcapStack_map.setDialogIdleTimeout(PropertyReader.GetPropertyAsInt("DialogIdleTimeout_map"));
            this.tcapStack_map.start();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
       
        logger.debug("Initialized TCAP Stack ....");
    }

   

    private void initMAP() {
        logger.debug("Initializing MAP Stack ....");
        this.mapStack = new MAPStackImpl("map_stack", tcapStack_map.getProvider());
        Main.mapProvider = this.mapStack.getMAPProvider();
        MapHandling instance = new MapHandling(Main.mapProvider);
        Main.mapProvider.addMAPDialogListener(instance);
        //Activate MAP SMS Service for SRI, Submit-SM
        Main.mapProvider.getMAPServiceSms().addMAPServiceListener(instance);
        Main.mapProvider.getMAPServiceSms().acivate();
        //Activate MAP Mobility service for ATI
        Main.mapProvider.getMAPServiceMobility().addMAPServiceListener(instance);
        Main.mapProvider.getMAPServiceMobility().acivate();
        //Activate MAP call handling service for SRI
        Main.mapProvider.getMAPServiceCallHandling().addMAPServiceListener(instance);
        Main.mapProvider.getMAPServiceCallHandling().acivate();

        Main.mapProvider.getMAPServiceSupplementary().addMAPServiceListener(instance);
        Main.mapProvider.getMAPServiceSupplementary().acivate();

        try {
            this.mapStack.start();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }

        logger.debug("Initialized MAP Stack ....");
    }
}
