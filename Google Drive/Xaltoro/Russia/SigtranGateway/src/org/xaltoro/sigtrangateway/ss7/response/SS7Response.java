package org.xaltoro.sigtrangateway.ss7.response;

import java.util.logging.Level;
import org.xaltoro.sigtrangateway.Main;
import org.xaltoro.sigtrangateway.ss7.MapHandling;
import org.xaltoro.sigtrangateway.ss7.SS7Operation;
import org.xaltoro.sigtrangateway.ss7.request.SS7Requst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class SS7Response extends SS7Operation implements Runnable {

    public Long DialogId;
    public String refId;

    @Override
    public abstract void run();

    public abstract String encode();
}
