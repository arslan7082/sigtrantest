package org.xaltoro.sigtrangateway.utils;

import com.rockville.utilities.XMLReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xaltoro.sigtrangateway.ss7.request.AnytimeInterrogationRequest;
import org.xaltoro.sigtrangateway.ss7.request.SS7Requst;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ali Khan
 */
public class DoATI {

    private static org.slf4j.Logger logger = LoggerFactory.getLogger("application");
    SS7Requst atiReq;
    final Object latch;
    String rawResponse;
    final String refID;
    boolean atiResponseRecieved = false;

    public DoATI(String refID, String xml) {
        atiReq = new AnytimeInterrogationRequest();
        atiReq.RawIncomingXml = xml;
        this.refID = refID;
        atiReq.ReturnXML = "";
        latch = new Object();
        rawResponse = "";
    }

    /**
     * Does an ATI Request asynchrounously
     */
    public void Now() {
        new Thread() {
            public void run() {
                atiReq.run();
                synchronized (atiReq) {
                    try {
                        atiReq.wait(2000);
                        rawResponse = atiReq.ReturnXML;
                        logger.info("[" + refID + "] ATI Response recieved: " + rawResponse);
                        atiResponseRecieved = true;
                        atiReq.notify();
                    } catch (InterruptedException ex) {
                        logger.error("Error waiting on atiReq", ex);
                    }
                }
            }
        }.start();
    }

    public ATIState NowSync() {
        ATIState returnedState = new ATIState();
        atiReq.run();
        synchronized (atiReq) {
            try {
                atiReq.wait(3000);
                rawResponse = atiReq.ReturnXML;
                if(!atiReq.isComplete){
                    logger.info("[" + refID + "]ATI Response not received in 3 sec");
                }
                logger.info("[" + refID + "] ATI Response received: " + rawResponse);
                atiResponseRecieved = atiReq.isComplete;
                returnedState = getState();
            } catch (InterruptedException ex) {
                logger.error("[" + refID + "]Error waiting on atiReq", ex);
            }
        }
        return returnedState;

    }

    public String GetRawResponse() {
        return rawResponse;
    }

    /**
     * Gets the state of the ATI performed, waits for the ATI response for max
     * period of timeout specified, returns null if response not arrived in
     * specified time
     *
     * @param timeout the time to wait for response in milliseconds
     * @return
     */
    public ATIState getState(long timeout) {
        synchronized (atiReq) {
            //while (!atiResponseRecieved) 
            {
                try {
                    atiReq.wait(timeout);
                } catch (InterruptedException ex) {
                    logger.error("Error waiting on atiReq while trying to get aitState", ex);
                }
            }
        }
        if ("".equals(this.rawResponse)) {
            //Response not recieved yet
            logger.info("[" + refID + "] ATI Response not recived yet");
            return null;
        }

        XMLReader xReader = new XMLReader();
        xReader.loadXml(this.rawResponse);
        String atiCamelState = xReader.readOneElement("substate");
        String aol = "";
        aol = xReader.readOneElement("ageOfLocation");
        logger.info("[" + refID + "] ATI State: " + atiCamelState + " AOL: " + aol);
        atiCamelState = atiCamelState.trim();

        ATIState atiState = new ATIState();
        atiState.setAgeOfLocation(aol);

        switch (atiCamelState) {
            case "assumedIdle":
                atiState.AtiCamelState = CamelState.AssumedIdle;
                break;
            case "camelBusy":
                atiState.AtiCamelState = CamelState.CamelBusy;
                break;
            case "netDetNotReachable":
                atiState.AtiCamelState = CamelState.NotReachable;
                break;
            default:
                atiState.AtiCamelState = CamelState.Error;
        }
        logger.info("[" + refID + "] ATI xml " + atiState.toString());
        return atiState;
    }

    public ATIState getState() {
        ATIState atiState = new ATIState();
        if ("".equals(this.rawResponse)) {
            //Response not recieved yet
            logger.info("[" + refID + "] ATI Response not recived");
            return atiState;
        }

        XMLReader xReader = new XMLReader();
        xReader.loadXml(this.rawResponse);
        String atiCamelState = xReader.readOneElement("substate");
        String aol = "";
        aol = xReader.readOneElement("ageOfLocation");
        logger.info("[" + refID + "] ATI State: " + atiCamelState + " AOL: " + aol);
        atiCamelState = atiCamelState.trim();

        
        atiState.setAgeOfLocation(aol);

        switch (atiCamelState) {
            case "assumedIdle":
                atiState.AtiCamelState = CamelState.AssumedIdle;
                break;
            case "camelBusy":
                atiState.AtiCamelState = CamelState.CamelBusy;
                break;
            case "netDetNotReachable":
                atiState.AtiCamelState = CamelState.NotReachable;
                break;
            default:
                atiState.AtiCamelState = CamelState.Error;
        }
        logger.info("[" + refID + "] ATI xml " + atiState.toString());
        return atiState;
    }
}
