package org.xaltoro.sigtrangateway;


import com.rockville.properties.PropertyNotFoundException;
import com.rockville.properties.PropertyReader;
import com.rockville.socket.TcpClient;
import com.rockville.socket.TcpRequestResult;
import com.rockville.utilities.XMLReader;
import java.util.Date;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Arslan subAuthor SHoaib
 *
 */
public class ProvgwProxy {

    private static Logger logger = LoggerFactory.getLogger("application");
    private String provgwIP;
    private int provgwPort;
    private int connectTimeout;
    private int readTimeout;

    public ProvgwProxy() throws PropertyNotFoundException {
        
          //  Properties myResources = new Properties();
           // myResources.load(new FileInputStream("mapgw.properties"));
            provgwIP = PropertyReader.GetProperty("provgw.ip");// myResources.getProperty("provgw.ip");
            provgwPort = PropertyReader.GetPropertyAsInt("provgw.port");// Integer.parseInt(myResources.getProperty("provgw.port"));
            connectTimeout =PropertyReader.GetPropertyAsInt("tcp.connectTimeoutMilliseconds");// Integer.parseInt(myResources.getProperty("tcp.connectTimeoutMilliseconds"));
            readTimeout =PropertyReader.GetPropertyAsInt("tcp.readTimeoutMilliseconds");// Integer.parseInt(myResources.getProperty("tcp.readTimeoutMilliseconds"));
        
    }

    /**
     *
     * @param msisdn
     * @param B_msisdn
     * @return The response message recieved from provGW
     */
    public void SendStatus(String CalledParty, String CallingParty, String refId) {
        String formattedMsg = "<methodCall>"
                + "<function>sendSubscriberStatus</function>"
                + "<bpartyMSISDN>" + CalledParty + "</bpartyMSISDN>"
                + "<apartyMSISDN>" + CallingParty + "</apartyMSISDN>"
                + "<refID>" + refId + "</refID>" + "<callDateTime>"
                + new Date().toString() + "</callDateTime>" + "</methodCall>";
        TcpClient tcpClient = new TcpClient();
        tcpClient.sendRequest(provgwIP, provgwPort,
                formattedMsg, refId, connectTimeout, readTimeout, logger);
    }

    private boolean isPostPaid(String MSISDN, String refId) {
        logger.info("Checking for " + MSISDN + " for prepaid/postpaid");
        String formattedMsg = "<methodCall>"
                + "<function>querySubscriber</function>" + "<bpartyMSISDN>"
                + MSISDN + "</bpartyMSISDN>" + "</methodCall>";
        TcpClient tcpClient = new TcpClient();
        TcpRequestResult rr = tcpClient.sendRequest(provgwIP, provgwPort,
                formattedMsg, refId, connectTimeout, readTimeout, logger);
        String response = rr.responseString;

        XMLReader xRead = new XMLReader();
        xRead.loadXml(response);

        String resRefID = xRead.readOneElement("RefID");
        String subType = xRead.readOneElement("subType");
        if (subType == "1") {
            return true;
        }

        return false;
    }
}