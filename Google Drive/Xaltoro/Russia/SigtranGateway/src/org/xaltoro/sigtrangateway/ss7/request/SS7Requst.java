package org.xaltoro.sigtrangateway.ss7.request;

import org.xaltoro.sigtrangateway.ss7.SS7Operation;

public abstract class SS7Requst extends SS7Operation implements Runnable {

    public String RawIncomingXml;
    public String ReturnXML;
    public boolean isComplete;
    public String refId;
    public String MSISDN;
    public String aMSISDN;
    public String bMSISDN;

    @Override
    public abstract void run();

    public abstract void decode();
}
