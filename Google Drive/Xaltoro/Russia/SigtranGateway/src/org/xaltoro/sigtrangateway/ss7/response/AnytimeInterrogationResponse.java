package org.xaltoro.sigtrangateway.ss7.response;

import org.xaltoro.sigtrangateway.Main;
import org.xaltoro.sigtrangateway.ss7.request.AnytimeInterrogationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pc
 */
public class AnytimeInterrogationResponse extends SS7Response {

    public String resultCode;
    public String vlr;
    public String subState;
    public String msisdn;
    public String ageOfLocation;
    public int cellId;
    public int mcc;
    public int mnc;
    public int lac;
    private static Logger logger = LoggerFactory
            .getLogger("application");
    private static Logger cdr_writer = LoggerFactory.getLogger("cdrs");

    @Override
    public void run() {
        AnytimeInterrogationRequest ss7Request = (AnytimeInterrogationRequest) Main.HT_SS7Requests.get(DialogId);
        ss7Request.isComplete = true;
        this.refId = ss7Request.refId;
        this.msisdn = ss7Request.MSISDN;
        ss7Request.ReturnXML = encode();
        logger.info("[" + this.refId + "] ATI Response recieved: " + ss7Request.ReturnXML);
        cdr_writer.info(this.refId + "," + this.msisdn + ",IN,ATI-RESPONSE," + subState + "," + ageOfLocation);
        synchronized (ss7Request) {
            ss7Request.notify();
        }
        Main.HT_SS7Requests.remove(DialogId);
    }
//<Response><result>1</result><RefID>null</RefID><resultCode>null</resultCode><msisdn>null</msisdn><vlr>923330052011</vlr><substate>assumedIdle</substate><cellid>12022</cellid><mcc>410</mcc><mnc>3</mnc><lac>58808</lac></Response>

    @Override
    public String encode() {
        String response = "<Response>" + "<result>1</result>" + "<RefID>" + refId
                + "</RefID>" + "<resultCode>" + resultCode + "</resultCode>"
                + "<msisdn>" + msisdn + "</msisdn>" + "<vlr>" + vlr + "</vlr>"
                + "<substate>" + subState + "</substate>" + "<cellid>" + cellId
                + "</cellid>" + "<mcc>" + mcc + "</mcc>" + "<mnc>" + mnc
                + "</mnc>" + "<lac>" + lac + "</lac>"
                + "<ageOfLocation>" + ageOfLocation + "</ageOfLocation>"
                + "</Response>";
        return response;
    }
}
